package com.anderson.study.controller;

import com.anderson.study.service.HeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HeroController {
    @Autowired
    HeroService heroService;

    @RequestMapping("/home")
    public String getIndex(){

        return "welcome";
    }


    @ResponseBody
    @GetMapping(value = "/hello")
    public String home () {
        return "hello my dear;";
//        return heroService.findByUsername("roma").getUsername();

    }
}
