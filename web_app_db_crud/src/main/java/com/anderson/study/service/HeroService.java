package com.anderson.study.service;

import com.anderson.study.entity.Hero;
import com.anderson.study.repository.HeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HeroService {
    @Autowired
    HeroRepository heroRepository;


    public Hero findByUsername(String username) {
        return heroRepository.findByUsername(username);
    }
}
