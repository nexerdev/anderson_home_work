package com.anderson.study;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    List<Person> listPerson2 =  new ArrayList<>();

    public static void main(String[] args) {
        List<String> emptyList = new ArrayList<>();
        List<String> listString = List.of("Subzero", "Goro", "3 - element", "Goro", "Sony", "Goro", "Raiden", "Reptile", "Scorpion", "Jax", "Goro" , "jax", "goro");
        List<Person> listPerson =  new ArrayList<>();
        listPerson.add(new Person("Subzero",28,"Man"));
        listPerson.add(new Person("Subzero",21,"Man"));
        listPerson.add(new Person("Goro",110,"Man"));
        listPerson.add(new Person("Goro",22,"Man"));
        listPerson.add(new Person("Jax",78,"Man"));
        listPerson.add(new Person("Sony",21,"Woman"));
        listPerson.add(new Person("Sony2",22,"Woman"));
        listPerson.add(new Person("Raiden",28,"Man"));
        listPerson.add(new Person("Ivan",66,"Man"));
        listPerson.add(new Person("Ivan",13,"Man"));
        listPerson.add(new Person("Anna",59,"Woman"));



        System.out.println("||||||||||||||||   FIRST PART   ||||||||||||||||");
        System.out.println("\n----------------List String----------------");
        String[] strings = listString.stream().map(s -> s.toUpperCase()).distinct().toArray(String[]::new);
        Arrays.stream(strings).forEach(x -> System.out.print(x + " | "));


        System.out.println("\n\n----------------First and Last element---------------------");
        Optional<String> first = listString.stream().findFirst();
        Optional<String> last =  listString.stream().reduce((f, s) -> s);
        System.out.printf("First:%s; Last:%s;\n" ,first,last);


        System.out.println("\n--------------Find a3-----------------------");
        try {
            String a3 = listString.stream().filter(x -> x.equals("a3")).findAny().get();
            System.out.println(a3);
        }catch (Exception e){
            System.out.println(e);
        }


        System.out.println("\n----------------Third Element---------------------");
        String thirdElement = listString.stream().skip(2).findFirst().get();
        System.out.println(thirdElement);
        System.out.println("\n----------------Two element. Start from element 2---------------------");
        listString.stream().skip(1).limit(2).forEach(System.out::println);




        System.out.println("\n\n||||||||||||||||   SECOND PART   ||||||||||||||||");

        System.out.println("\n---------------Filter by name----------------------");
        listPerson.stream().filter(p -> p.getName().equals("Jax")).forEach(p -> System.out.println(p.getName()));

        System.out.println("\n---------------Quantity Ivan----------------------");
        long countVal = listPerson.stream().filter(p -> p.getName().equals("Ivan")).count();
        System.out.println(countVal);

        System.out.println("\n---------------Length of Name----------------------");
        listPerson.stream().forEach(p  -> System.out.print(p.getName().length()+ " | "));

        System.out.println("\n\n---------------Convert one collect to another----------------------");
        Set<Person> collect = listPerson.stream().collect(Collectors.toSet());
        System.out.println(collect.getClass());

        System.out.println("\n---------------Men 27 > age > 18----------------------");
        listPerson.stream()
                .filter(p -> p.getSex().equals("Man") &&   p.getAge() > 18 && p.getAge() < 28 )
                .forEach(p -> System.out.println(p.getName()));

        System.out.println("\n---------------AVG age men----------------------");
        OptionalDouble ageAVG = listPerson.stream()
                .filter(p -> p.getSex().equals("Man"))
                .mapToInt(p -> p.getAge()).average();
        System.out.println(ageAVG);

        System.out.println("\n---------------Workable----------------------");
        listPerson.stream()
                .filter(p -> p.getAge() > 18)
                .filter(p -> ( p.getSex().equals("Woman") && p.getAge() < 55) || (p.getSex().equals("Man") && p.getAge() < 60))
                .forEach(p-> System.out.printf( "Name:%s Age:%s\n" ,p.getName(), p.getAge()));

        System.out.println("\n---------------Add value to every man----------------------");
        listPerson.stream()
                .filter(p -> p.getSex().equals("Man"))
                .map(p -> p.getName() + " IS WEAK")
                .forEach(System.out::println);
    }
}

